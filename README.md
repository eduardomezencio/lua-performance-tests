# lua-performance-tests

Tests to compare lua and luajit performance in some scenarios, focused on the
effect of simple 'optimizations' using local temporary variables.

## To run

Have lua, luajit and love in your PATH. Have the time command installed.

    # in lua-luajit folder
    sh run.sh
    
    # in love folder
    love . [case]  # e.g. `love . 1` runs case 1
