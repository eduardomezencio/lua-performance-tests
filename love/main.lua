local TIMES = 100000000

local case = {}


case[1] = function()
    local t, w, h

    local getWidth = love.graphics.getWidth
    local getHeight = love.graphics.getHeight
    t = love.timer.getTime()
    for i = 1, TIMES do
        w = getWidth()
        h = getHeight()
    end
    t = love.timer.getTime() - t
    print('local functions: ' .. t)

    local graphics = love.graphics
    t = love.timer.getTime()
    for i = 1, TIMES do
        w = graphics.getWidth()
        h = graphics.getHeight()
    end
    t = love.timer.getTime() - t
    print('local graphics: ' .. t)

    t = love.timer.getTime()
    for i = 1, TIMES do
        w = love.graphics.getWidth()
        h = love.graphics.getHeight()
    end
    t = love.timer.getTime() - t
    print('nothing local: ' .. t)

    love.event.quit()
end


case[2] = function()
    local sin = math.sin
    local s, t

    t = love.timer.getTime()
    for i = 1, TIMES do
        s = sin(i)
    end
    t = love.timer.getTime() - t
    print('sin local: ' .. t)

    t = love.timer.getTime()
    for i = 1, TIMES do
        s = math.sin(i)
    end
    t = love.timer.getTime() - t
    print('nothing local: ' .. t)

    love.event.quit()
end


case[3] = function()
    local x, y, w, z, t = -2, 2, nil, nil

    t = love.timer.getTime()
    for i = 1, TIMES do
        w = x < 0 and -x or x
        z = y < 0 and -y or y
    end
    t = love.timer.getTime() - t
    print('and or: ' .. t)

    t = love.timer.getTime()
    for i = 1, TIMES do
        w = math.abs(x)
        z = math.abs(y)
    end
    t = love.timer.getTime() - t
    print('math.abs: ' .. t)

    local abs = math.abs
    t = love.timer.getTime()
    for i = 1, TIMES do
        w = abs(x)
        z = abs(y)
    end
    t = love.timer.getTime() - t
    print('local abs: ' .. t)

    love.event.quit()
end


case[4] = function()
    local t = 0
    local v = {1, 2, 3, 4, 5, 6}

    local function f(a, b, c, d, e, f)
        return a + b + c + d + e + f
    end

    t = love.timer.getTime()
    local n = 0
    for _ = 1, TIMES do
        n = n + f(unpack(v))
    end
    t = love.timer.getTime() - t
    print('Using unpack: ' .. t)

    t = love.timer.getTime()
    local n = 0
    for _ = 1, TIMES do
        n = n + f(v[1], v[2], v[3], v[4], v[5], v[6])
    end
    t = love.timer.getTime() - t
    print('Manual: ' .. t)

    love.event.quit()
end


function love.load(arg)
    love.update = case[tonumber(arg[1] or '1')]
    if arg[2] then TIMES = arg[2] end
end
