if arg[1] == 'no' then
  local obj = {pos = {0, 1}}
  for _ = 1, arg[2] do
    obj.pos[1] = obj.pos[1] + obj.pos[2]
    obj.pos[2] = obj.pos[1] + obj.pos[2]
  end
end

if arg[1] == 'yes' then
  local obj = {pos = {0, 1}}
  local pos = obj.pos
  for _ = 1, arg[2] do
    pos[1] = pos[1] + pos[2]
    pos[2] = pos[1] + pos[2]
  end
end
