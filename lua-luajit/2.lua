local x

if arg[1] == 'no' then
  for i = 1, arg[2] do
    x = math.sin(i)
  end
end

if arg[1] == 'yes' then
  local sin = math.sin
  for i = 1, arg[2] do
    x = sin(i)
  end
end
