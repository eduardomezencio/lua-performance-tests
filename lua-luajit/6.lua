local a, i, j = {{1, 2, 3}, {4, 5, 6}}, 1, 2

if arg[1] == 'no' then
  for _ = 1, arg[2] do
    a[i][j] = a[i][j] * a[i][j + 1]
  end
end

if arg[1] == 'yes' then
  local k = a[i]
  for _ = 1, arg[2] do
    k[j] = k[j] * k[j + 1]
  end
end
