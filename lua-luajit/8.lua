local unpack = unpack and unpack or table.unpack
local t = {1, 2, 3, 4, 5, 6}


local function f(a, b, c, d, e, f)
  return a + b + c + d + e + f
end


if arg[1] == 'no' then
  local n = 0
  for _ = 1, arg[2] do
    n = n + f(unpack(t))
  end
end

if arg[1] == 'yes' then
  local n = 0
  for _ = 1, arg[2] do
    n = n + f(t[1], t[2], t[3], t[4], t[5], t[6])
  end
end
