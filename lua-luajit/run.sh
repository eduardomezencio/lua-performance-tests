#!/bin/sh

FORMAT="%U (%E)"
TIMES=${1:-100000000}

CASE1='Store nested table in local variable.'
CASE2='Store library function in local variable.'
CASE3='Store nested table in local variable each iteration.'
CASE4='LuaJIT docs CSE by hand 1'  # Common Subexpression Elimination
CASE5='LuaJIT docs CSE by hand 1 (each iteration)'
CASE6='LuaJIT docs CSE by hand 2'
CASE7='LuaJIT docs CSE by hand 2 (each iteration)'
CASE8='Unpack manual vs auto'

for CASE in $(seq 1 8); do
    eval "echo \$CASE$CASE"
    for COMMAND in lua luajit; do
        printf "%8s: No:  " "$COMMAND"
        command time -f "$FORMAT" "$COMMAND" "$CASE.lua" no "$TIMES"
        printf "%8s  Yes: " ""
        command time -f "$FORMAT" "$COMMAND" "$CASE.lua" yes "$TIMES"
        echo
    done
done
